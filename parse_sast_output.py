import sys, string
import json

color = {
    "MAGENTA" : '\033[95m',
    "CYAN" : '\033[94m',
    "YELLOW" : '\033[93m',
    "GREEN" : '\033[92m',
    "RED" : '\033[91m',
    "UNDERLINE" : '\033[4m',
    "ENDC" : '\033[0m',
}

tpl = """
 message : ${MAGENTA}${message}${ENDC} 
 severity : ${GREEN}${severity}${ENDC}
 filename : ${RED}${filename}${ENDC}
 start_line : ${YELLOW}${start_line}${ENDC}"""

def parse_json(json_file):
	f = open(json_file, 'r')
        data = json.loads(f.read())
        for i in data['vulnerabilities']:
		s = tpl
		s = string.Template(s).safe_substitute(color)
    		s = string.Template(s).safe_substitute(message=i['message'])
    		s = string.Template(s).safe_substitute(severity=i['severity'])
    		s = string.Template(s).safe_substitute(filename=i['location']['file'])
		s = string.Template(s).safe_substitute(start_line=str(i['location']['start_line']))
                print(s)
		print(' ')
        f.close()    


if __name__ == '__main__':    
    if len(sys.argv) < 2:
        print('Argument error')
        print('Usage: $>python parse.py <json-file>')
        sys.exit()
    else:
        parse_json(sys.argv[1])
    
    sys.exit()